slug:en
title: Saint-Petersburg Linux Users group

## About the group

Saint Petersburg Linux Users Group (SPbLUG) is informal community of GNU/Linux
and OpenSource software users in Saint Petersburg and Leningrad region,
Russia.

## Information resources

- [Maillist](http://groups.google.com/group/spblinux/) on the
  groups.google.com server;
- FIDO-conference SPB.LINUX (read-only access via [Google
  groups](http://groups.google.com/group/fido7.spb.linux));
- IRC-channel [#spblug](http://webchat.freenode.net/?channels=spblug&uio=d4)
  on the [FreeNode](http://freenode.net/) network
  [servers](http://freenode.net/irc_servers.shtml);
- [Telegram News](https://t.me/spblug) channel;
- SPbLUG's [bloggers planet](http://planet.spblug.org/);
- [SPbLUG @ Google+](https://plus.google.com/109127630753609453011)

Please, feel free to contact us via e-mail <info@spblug.org> in case you have
questions or suggestions!
