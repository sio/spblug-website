save_as: index.html
url:
status: hidden
title: Санкт-Петербургская группа пользователей Linux

## О группе

Санкт-Петербургская группа пользователей Linux (Saint Petersburg Linux Users
Group, SPbLUG) — неформальное объединение пользователей GNU/Linux и других
OpenSource систем на территории Санкт-Петербурга и Ленинградской области.

## Ресурсы

- [Список рассылки](http://groups.google.com/group/spblinux/) на сервере
  groups.google.com;
- FIDO-конференция SPB.LINUX (доступна для чтения через
  [веб-интерфейс](http://groups.google.com/group/fido7.spb.linux) Google);
- IRC-канал [#spblug](http://webchat.freenode.net/?channels=spblug&uio=d4) на
  [серверах](http://freenode.net/irc_servers.shtml) сети
  [FreeNode](http://freenode.net/);
- [Telegram News](https://t.me/spblug) channel;
- [Планета блогов](http://planet.spblug.org/) SPbLUG;
- [SPbLUG @ Google+](https://plus.google.com/109127630753609453011)

Если вы не нашли ответа на свой вопрос в вышеприведённых местах не стесняйтесь
его задать по e-mail <info@spblug.org>!

## Календарь событий

<iframe src="https://www.google.com/calendar/embed?title=%D0%A0%D0%B0%D1%81%D0%BF%D0%B8%D1%81%D0%B0%D0%BD%D0%B8%D0%B5%20%D0%B1%D0%BB%D0%B8%D0%B6%D0%B0%D0%B9%D1%88%D0%B8%D1%85%20%D1%81%D0%BE%D0%B1%D1%8B%D1%82%D0%B8%D0%B9%20SPbLUG&amp;showTitle=0&amp;showNav=0&amp;showDate=0&amp;showTabs=0&amp;showCalendars=0&amp;mode=AGENDA&amp;height=300&amp;wkst=2&amp;hl=ru&amp;bgcolor=%23ffffff&amp;src=bhjftd3c7s8n0h38n7r5m0ghmo%40group.calendar.google.com&amp;color=%23A32929&amp;ctz=Europe%2FMoscow" style=" border-width:0 " width="800" height="300" frameborder="0" scrolling="no"></iframe>
